import React, { useState } from 'react';
function App() {

  const [name, setName] = useState("");
  const [result, setResult] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();
    fetch(
      `https://${process.env.REACT_APP_SUBDOMAIN}.${process.env.REACT_APP_DOMAIN}/superheroes/identify/${name}`
    )
      .then(function (response) {
        return response.json();
      })
      .then(function (data) {
        setResult(data.result)
        setName("");
      })
      .catch(function (error) {
        console.log("Error in identifying the superhero");
      });
  };

  return (
    <div className="App"  >
      <h2>Unmask that superhero!</h2>
      <form onSubmit={handleSubmit}>
        <label>Name: </label>
        <input type="text" onChange={(e) => setName(e.target.value)} />
        <button type="submit">Reveal</button>
        <h2>{ result }</h2>
      </form>
    </div>
  );
}

export default App;